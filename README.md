# android_vendor_xiaomi_haydn-firmware

Firmware images for Redmi K40 Pro/Redmi K40 Pro+/Mi 11i/Mi 11X Pro (haydn), to include in custom ROM builds.

**Current version**: fw_haydn_miui_HAYDN_V14.0.3.0.TKKCNXM_1bd4117cae_13.0

### How to use?

1. Clone this repo to `vendor/xiaomi/haydn-firmware`

2. Include it from `BoardConfig.mk` in device tree:

```
# Firmware
-include vendor/xiaomi/haydn-firmware/BoardConfigVendor.mk
```
